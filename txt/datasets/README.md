# p-tags
- Data
    - Breakdown (data15/16/17/18)
        - `p3704 ( 66/151/185/192)`
- MC Background
    - Breakdown (mc16a/d/e)
        - `p3703 (178/178/158)`
        - `p3736 (  4/  2/  9)`
        - `p3864 (  0/  0/  3)`
        - `p3815 (  0/  0/  2)`
        - `p3780 (  0/  0/  2)`
        - `p3759 (  0/  2/  2)`
        - `p3875 (  0/  0/  1)`
- MC Stop2L 3-body Signal (mc16a/d/e)
    - Breakdown
        - `p3759 ( 36/ 36/ 36)`
- MC LFV Signal (mc16a/d/e)
    - Breakdown
        - `p3759 (  6/  6/  6)`

# Problematic SusyNts

# Missing PRWs 
PRWs looked for at `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/`

# Problematic DAODs
`data17_13TeV.00331710.physics_Main.merge.AOD.r10203_p3399`

`data18_13TeV.00351894.physics_Main.merge.AOD.f938_m1979`

`data18_13TeV.00360063.physics_Main.merge.AOD.f969_m2020`
- Data samples in GRL are not listed as "ALL EVENTS AVAILABLE" (see https://its.cern.ch/jira/browse/ATLSUSYDPD-1970). Using anyway

`mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY2.e6337_s3126_r10724_p3703`
- listed as 99.79% complete on AMI. Using anyway

436200, 436201, 436202, 436204, 436205, 436206, 436208 436209
- Stop2L signal samples missing the mc16e campaign. Need to request production

`gamma_pty_7_15`
- Missing mc16e campaign for all Z+gamma samples with photon pt slice 7-15GeV. Ignoring for now

# Missing AODs
