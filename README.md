susynt-submit
=============


# Contents

* [Introduction](#introduction)
* [Requirements](#requirements)
* [Actions](#actions)
    * [Basic Setup](#basic-setup) 
    * [Launching a Grid Job](#launching-a-grid-job)
* [Useful Scripts](useful-scripts)

# Introduction

Files and scripts needed to submit a SusyNt ntuple production

# Requirements

1) Setup the SusyNt environment as described in [UCINtSetup](https://gitlab.cern.ch/alarmstr/UCINtSetup)
2) Setup GRID certificate (`voms-proxy-init -voms atlas -valid 144:00`) 
3) Setup Panda (`lsetup panda`)

# Actions

## Basic setup

```bash
cd source/
git clone https://gitlab.cern.ch/susynt/susynt-submit.git
cd ../run
mkdir submitSusyNt
cd submitSusyNt
ln -s ../../source/susynt-submit/bash
ln -s ../../source/susynt-submit/txt
ln -s ../../source/susynt-submit/python
cd ../..
```

## Launching a grid job

Package up your submission and build directories
```bash
cd run/submitSusyNt
lsetup panda
./bash/create_tarball.sh
```
The resulting tar file, `area.tgz`, should contain everything in the `build/x86_64*/` and `submitSusyNt/` directory, including symlinks.
This is important because the job will assume there is `bash` directory with specific files to execute.
Also, make sure there are no large files that get included in the tar file as this will slow down jobs or potentially stop them from running. 

At this point you can submit the jobs for the samples you need.
These sample DIDs should be placed in a txt file and then commands like those below can be run:
```bash
./python/submit.py --nickname ${USER} -t ${TAG} -f txt/datasets/2015/mc_p1784.txt 2>&1 | tee production.log
./python/submit.py --trigFilter --nickname ${USER} -t ${TAG} -f txt/datasets/2015/data_egamma.txt 2>&1 | tee production.log
```

The submit.py script assumes there is a link to the [`txt/`](txt/) directory.

Notes:
- for data it is useful to only write events that pass our triggers,
  `--filterTrig`; the reduction in simulated samples is probably not worth it
- `${USER}` and `${TAG}` have to be specified. `${USER}` must match your grid username.
- The script will provide output (e.g. the NtMaker options) for each job as it is being submitted
- The final output name of the sample will be what is shown in the script output for the `--output` option with `\` replaced by `_nt` (e.g. `sample_name_n0308/` -> `sample_name_n0308_nt`)

## Launching a condor job

The important files are [submitFile.condor](bash/submitFile.condor) and [submit_condor.py](python/submit_condor.py) but these are deprecated and require some fixin'

# Useful scripts
[getProdStatus.sh](bash/getProdStatus.sh) - 
process samples listed in a txt file to determine there production status from AMI

[bigpanda_monitoring.py](python/bigpanda_monitoring.py) - 
process the log file from running submit.py to get a url link for the job status of each job in the log file

[prepare_lists.py](python/prepare_lists.py) - Note sure how to use it

[get_failed_condor_jobs.py](python/get_failed_condor_jobs.py) - Note sure how to use it

[resubmit_failed_jobs.py](python/resubmit_failed_jobs.py) - Note sure how to use it