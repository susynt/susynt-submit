#!/bin/env python

from __future__ import print_function
import os, sys, glob, json
import subprocess
import argparse
import time
import threading
import csv

def mc_campaigns() :
    return ['mc16a', 'mc16d', 'mc16e']

def build_data_lists(year, container_list, args) :

    fname = 'data_did_list_data{}'.format(year)
    if args.suffix != '' :
        fname += '_{}'.format(args.suffix.strip())
    fname += '.txt'

    outdir = os.path.abspath(args.outdir)
    if not os.path.isdir(outdir) :
        print('Making output directory {}'.format(outdir))
        cmd = 'mkdir -p {}'.format(outdir)
        subprocess.call(cmd, shell = True)
    fname = '{}/{}'.format(outdir, fname)

    if args.use_max_ptag :
        tmp = []
        ptags = []
        for c in container_list :
            tag_string = get_tag_string_from_container(c)
            ptag_num = get_ptag_from_tag_string(tag_string)
            ptags.append(ptag_num)
        max_ptag = max(ptags)

        for c in container_list :
            ptag_str = 'p{}'.format(str(max_ptag))
            if ptag_str in c :
                tmp.append(c)

        container_list = tmp

    with open(fname, 'w') as f :
        for c in container_list :
            f.write('{}\n'.format(c))

def build_process_list(process_name, container_list, args) :

    tmp_name = 'mc_did_list_{}'.format(process_name)
    if args.suffix != '' :
        tmp_name += '_{}'.format(args.suffix.strip())
    tmp_name += '.txt'

    outdir = os.path.abspath(args.outdir)
    if not os.path.isdir(outdir) :
        print('Making output directory {}'.format(outdir))
        cmd = 'mkdir -p {}'.format(outdir)
        subprocess.call(cmd, shell = True)
    tmp_name = '{}/{}'.format(outdir, tmp_name)

    n_dids = len(container_list)

    if args.use_max_ptag :
        tmp = []
        ptags = { 'mc16a' : [], 'mc16d' : [], 'mc16e' : [] }
        for c in container_list :
            tag_string = get_tag_string_from_container(c)
            ptag_num = get_ptag_from_tag_string(tag_string)
            if 'r9364' in tag_string :
                ptags['mc16a'].append(ptag_num)
            elif 'r10201' in tag_string :
                ptags['mc16d'].append(ptag_num)
            elif 'r10724' in tag_string :
                ptags['mc16e'].append(ptag_num)

        max_ptag_a, max_ptag_d, max_ptag_e = '', '', ''

        if ptags['mc16a'] :
            max_ptag_a = str('p{}'.format(max(ptags['mc16a'])))
        if ptags['mc16d'] :
            max_ptag_d = str('p{}'.format(max(ptags['mc16d'])))
        if ptags['mc16e'] :
            max_ptag_e = str('p{}'.format(max(ptags['mc16e'])))

        for c in container_list :

            if 'r9364' in c and max_ptag_a :
                if max_ptag_a in c : tmp.append(c)
            if 'r10201' in c and max_ptag_d :
                if max_ptag_d in c : tmp.append(c)
            if 'r10724' in c and max_ptag_e :
                if max_ptag_e in c : tmp.append(c)

        container_list = tmp
    
    with open(tmp_name, 'w') as f :
        for c in container_list :
            f.write('{}\n'.format(c))

def year_from_container(container) :

    if 'data15_' in container : return '15'
    if 'data16_' in container : return '16'
    if 'data17_' in container : return '17'
    if 'data18_' in container : return '18'
    return ''

def get_tag_string_from_container(container) :

    tag_string = container.split('.')[-1].strip()
    return tag_string

def get_ptag_from_tag_string(tag_string) :

    tag_list = tag_string.split('_')
    last_tag = tag_list[-1]
    if not last_tag.startswith('p') :
        print('WARNING Last tag found is not p-tag (tag string = {})'.format(tag_string))
        return -1
    return int(last_tag.replace('p',''))

def has_expected_tag_format(tag_string, n_tags_exp = 4) :

    tag_list = [t.strip() for t in tag_string.split('_')]
    if len(tag_list) != n_tags_exp :
        return False
    return True

def in_requested_mc_campaign(tag_string, mcType) :

    keep_mc16a = False
    keep_mc16d = False
    keep_mc16e = False

    if args.mcType.lower() == 'all' :
        keep_mc16a = True
        keep_mc16d = True
        keep_mc16e = True
    elif args.mcType.lower() == 'mc16a' :
        keep_mc16a = True
    elif args.mcType.lower() == 'mc16d' :
        keep_mc16d = True
    elif args.mcType.lower() == 'mc16e' :
        keep_mc16e = True

    tags_for_campaign = { 'mc16a' : 'r9364',
                    'mc16d' : 'r10201',
                    'mc16e' : 'r10724' }

    keep_sample = False
    if keep_mc16a and tags_for_campaign['mc16a'] in tag_string :
        keep_sample = True
    if keep_mc16d and tags_for_campaign['mc16d'] in tag_string :
        keep_sample = True
    if keep_mc16e and tags_for_campaign['mc16e'] in tag_string :
        keep_sample = True

    return keep_sample

def get_process_container_names(did_list, all_containers) :

    container_names = []
    for did in did_list :
        dsid = did.split('.')[0].strip()
        for c in all_containers :
            if dsid in c :
                container_names.append(c)
    return container_names

def get_mc_lists(args) :

    if not os.path.isfile(args.json) :
        print('Provided JSON file (={}) not found'.format(args.json))
        sys.exit()

    with open(args.json) as f :
        mc_process_dict = json.load(f)

    loaded_processes = mc_process_dict.keys()
    n_proc = len(loaded_processes)

    if args.list_proc :
        for i, p in enumerate(loaded_processes) :
            print('Loading process [{0:02d}/{1:02d}]: {2}'.format(i+1, n_proc, p))
        sys.exit()

    if args.skip_proc != '' :
        skip_processes = [p.strip() for p in args.skip_proc.split(',')]
        tmp = []
        tmp_dict = {}
        for p in mc_process_dict :
            if p in skip_processes : continue
            tmp.append(p) 
            tmp_dict[p] = mc_process_dict[p]
        loaded_processes = tmp
        mc_process_dict = tmp_dict

    if args.sel_proc != '' :
        sel_processes = [p.strip() for p in args.sel_proc.split(',')]
        tmp = []
        tmp_dict = {}
        for p in mc_process_dict :
            if p not in sel_processes : continue
            tmp.append(p)
            tmp_dict[p] = mc_process_dict[p]
        loaded_processes = tmp
        mc_process_dict = tmp_dict

    tmp_all = 'all_mc_lists_all_{}.txt'.format(args.derivation.replace('DAOD_',''))
    make_file = True
    if args.use_existing_list and os.path.isfile(tmp_all) : 
        make_file = False
    if make_file :
        print('Building complete list of mc samples for derivation {}'.format(args.derivation.replace('DAOD_', '')))
        cmd = 'rucio ls mc16_13TeV.*.DAOD_{}.* |grep CONTAINER >> {}'.format(args.derivation.replace('DAOD_',''), tmp_all)
        subprocess.call(cmd, shell = True)
    else :
        print('Loading already built list of mc samples for derivation {}'.format(args.derivation.replace('DAOD_', '')))

    all_containers = []
    with open(tmp_all, 'r') as f :
        for line in f :
            line = line.strip()
            look_for = 'mc16_13TeV:'
            idx = line.find(look_for)
            container = line[idx + len(look_for):]
            container = container.strip()
            container = container.split('|')[0]

            keep_this_container = True

            tag_string = get_tag_string_from_container(container)
            if not has_expected_tag_format(tag_string) : continue
            if not in_requested_mc_campaign(tag_string, args.mcType) : continue
            ptag_number = get_ptag_from_tag_string(tag_string)

            if args.sel_ptag != '' :
                ptag_request = int(args.sel_ptag.replace('p','').strip())
                if ptag_number != ptag_number : continue
            
            if args.min_ptag :
                min_tag = args.min_ptag
                min_tag = int(min_tag.replace('p',''))
                if ptag_number < min_tag :
                    keep_this_container = False
            if args.max_ptag :
                max_tag = args.max_ptag
                max_tag = int(max_tag.replace('p',''))
                if ptag_number >= max_tag :
                    keep_this_container = False


            if not keep_this_container :
                continue
            all_containers.append(container)

    # remove duplicates
    all_containers = list(set(all_containers))

    jobs = []
    if args.verbose :
        print('Launching jobs from main thread {}'.format(threading.current_thread()))
    for p in loaded_processes :
        containers_for_process = get_process_container_names(mc_process_dict[p], all_containers)
        t = threading.Thread(target = build_process_list, args = (p, containers_for_process, args))
        t.start()
        jobs.append(t)

    for j in jobs :
        j.join()

def generate_data_list(args) :

    fname = 'all_data_list_{}.txt'.format(args.derivation.replace('DAOD_',''))
#    outdir = os.path.abspath(args.outdir)
#    if not os.path.isdir(outdir) :
#        print('Making output directory {}'.format(outdir))
#        cmd = 'mkdir -p {}'.format(outdir)
#        subprocess.call(cmd, shell = True)
#    fname = '{}/{}.txt'.format(outdir, fname)

    make_file = True
    if args.use_existing_list and os.path.isfile(fname) :
        make_file = False
    if make_file :
        print('Building complete list of data samples for derivation {}'.format(args.derivation.replace('DAOD_', '')))

        if args.verbose :
            print(' > 2015...')
        cmd = 'rucio ls data15_13TeV.*.DAOD_{}.* |grep CONTAINER >> {}'.format(args.derivation.replace('DAOD_', ''), fname) 
        subprocess.call(cmd, shell = True)
        if args.verbose :
            print(' > 2016...')
        cmd = 'rucio ls data16_13TeV.*.DAOD_{}.* |grep CONTAINER >> {}'.format(args.derivation.replace('DAOD_', ''), fname) 
        subprocess.call(cmd, shell = True)
        if args.verbose :
            print(' > 2017...')
        cmd = 'rucio ls data17_13TeV.*.DAOD_{}.* |grep CONTAINER >> {}'.format(args.derivation.replace('DAOD_', ''), fname) 
        subprocess.call(cmd, shell = True)
        if args.verbose :
            print(' > 2018...')
        cmd = 'rucio ls data18_13TeV.*.DAOD_{}.* |grep CONTAINER >> {}'.format(args.derivation.replace('DAOD_', ''), fname) 
        subprocess.call(cmd, shell = True)
    else :
        print('Loading already built list of data samples for derivation {}'.format(args.derivation.replace('DAOD_','')))

    return fname

def get_run_number_from_container(container) :

    c = container.strip()
    look_for = '13TeV.'
    idx = c.find(look_for)
    run = c[idx + len(look_for):]
    run = run.split('.')[0]
    run = run[2:]
    return run

def run_is_expected(container, runs_requested) :

    run_in = int(get_run_number_from_container(container))
    int_runs = [int(r) for r in runs_requested]
    if run_in in int_runs :
        return True
    return False

def get_data_lists(args) :

    lumi_table_file = args.lumi_table
    if not os.path.isfile(lumi_table_file) :
        print('ERROR Unable to find requested lumi table (={})'.format(lumi_table_file))
        sys.exit()

    data_list_name = generate_data_list(args)

    runs_to_get = []
    with open(lumi_table_file, 'r') as lumifile :
        run_idx = -1
        lumi_idx = -1
        for iline, line in enumerate(lumifile) :
            fields = [f.strip() for f in line.strip().split(',')]
            if iline == 0 :
                run_idx = fields.index('Run')
                lumi_idx = fields.index('Prescale Corrected')
                continue
            elif 'Total' in line : continue # skip the last line of the lumi tables
            else :
                if run_idx < 0 :
                    print('ERROR Could not parse lumi table CSV file correctly')
                    sys.exit()
                run_number = fields[run_idx].strip()
                runs_to_get.append(run_number)

    n_runs_expected = len(runs_to_get)
    if args.verbose :
        print('Loaded {} runs from input lumi-table (={})'.format(n_runs_expected, lumi_table_file))

    all_containers = []
    runs_obtained = []
    with open(data_list_name, 'r') as f :
        for line in f :
            line = line.strip()
            year = '15'
            if 'data16' in line : year = '16'
            elif 'data17' in line : year = '17'
            elif 'data18' in line : year = '18'
            look_for = 'data{}_13TeV:'.format(year)
            idx = line.find(look_for)
            container = line[idx + len(look_for):]
            container = container.strip()
            container = container.split('|')[0]

            if 'physics_Main' not in container :
                continue
            if 'period' in container :
                continue

            keep_this_container = True
            tag_string = get_tag_string_from_container(container)
            if not has_expected_tag_format(tag_string, 3) :
                continue
            ptag_number = get_ptag_from_tag_string(tag_string)

            if not run_is_expected(container, runs_to_get) :
                continue

            if args.sel_ptag != '' :
                ptag_request = int(args.sel_ptag.replace('p','').strip())
                if ptag_number != ptag_number : continue
            
            if args.min_ptag :
                min_tag = args.min_ptag
                min_tag = int(min_tag.replace('p',''))
                if ptag_number < min_tag :
                    keep_this_container = False
            if args.max_ptag :
                max_tag = args.max_ptag
                max_tag = int(max_tag.replace('p',''))
                if ptag_number >= max_tag :
                    keep_this_container = False

            if not keep_this_container :
                continue
            all_containers.append(container)
            runs_obtained.append(int(get_run_number_from_container(container)))

    # remove duplicates
    all_containers = list(set(all_containers))
    runs_obtained = list(set(runs_obtained))
    runs_obtained = sorted(runs_obtained)

    n_runs_obtained = len(runs_obtained)
    if n_runs_obtained != n_runs_expected :
        print('WARNING Did not find expected number of runs (expected: {}, obtained: {})'.format(n_runs_expected, n_runs_obtained))
        for expected_run in runs_to_get :
            expected_run = int(expected_run)
            if expected_run not in runs_obtained :
                print(' > Failed to get: {}'.format(expected_run))

    jobs = []
    if args.verbose :
        print('Launching jobs from main thread {}'.format(threading.current_thread()))

    containers_by_year = { '15' : [], '16' : [], '17' : [], '18' : [] }

    for run_number in runs_obtained :

        containers_for_run = []
        for container in all_containers :
            rstr = str(run_number)
            if rstr in container :
                containers_for_run.append(container)

        # handle duplicates by taking the most recent reprocessing
        container_to_use = containers_for_run[0]
        if len(containers_for_run) > 1 :
            first_tags = []
            for c in containers_for_run :
                tag_string = get_tag_string_from_container(container)
                first_tag = int(tag_string.split('_')[0].strip()[1:])
                first_tags.append(first_tag)
            max_first = max(first_tags)
            for c in containers_for_run :
                if str(max_first) in c :
                    container_to_use = c

        container = container_to_use
        
        year = year_from_container(container)
        if year == '' :
            print('ERROR Could not find year from input container name (={})'.format(container))
            sys.exit()
        rstr = str(run_number)
        if rstr in container :
            containers_by_year[year].append(container)

    for year in containers_by_year :
        if not containers_by_year[year] :
            continue
        build_data_lists(year, containers_by_year[year], args)

def main(args) :

    if args.json != '' :
        get_mc_lists(args)

    if args.lumi_table != '' :
        get_data_lists(args)

#_______________________
if __name__ == '__main__' :

    parser = argparse.ArgumentParser(
        description = 'Build a set of DAOD lists'
    )
    parser.add_argument('--json', default = '',
        help = 'Provide a process grouping list JSON file to use as DSID list'
    )
    parser.add_argument('--mcType', default = 'all',
        help = 'Set MC campaign (if generating MC lists) [mc16a, mc16d, mc16e] (default: all)'
    )
    parser.add_argument('-d', '--derivation', default = 'SUSY2',
        help = 'Which derivation to look for (default: SUSY2)'
    )
    parser.add_argument('--min-ptag', default = '',
        help = 'Set a minimum p-tag that all samples must have (inclusive)'
    )
    parser.add_argument('--max-ptag', default = '',
        help = 'Set a maximum p-tag that all samples must satisfy (non-inclusive)'
    )
    parser.add_argument('--use-max-ptag', action = 'store_true', default = False,
        help = 'Only keep the container with the maximum p-tag'
    )
    parser.add_argument('--sel-ptag', default = '',
        help = 'Select containers with this ptag'
    )
    parser.add_argument('--lumi-table', default = '',
        help = 'Get the data list for the provided lumi CSV table'
    )
    parser.add_argument('--list-proc', action = 'store_true', default = False,
        help = 'Only print out the MC process listing'
    )
    parser.add_argument('--skip-proc', default = '',
        help = 'Provide a list of MC processes to skip (comma-separated-list)'
    )
    parser.add_argument('--sel-proc', default = '',
        help = 'Provide a list of MC processes to build lists for (comma-separated-list)'
    )
    parser.add_argument('--use-existing-list', action = 'store_true', default = False,
        help = 'Do not regenerate the complete mc list if one is already found'
    )
    parser.add_argument('--suffix', default = '',
        help = 'Output suffix to any output files'
    )
    parser.add_argument('--outdir', default = './',
        help = 'Provide an output directory to dump outputs to (default: ./)'
    )
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False,
        help = 'Be more talkative'
    )
    args = parser.parse_args()

    if not args.json and not args.lumi_table :
        print('No lists (JSON or lumi-table) provided')
        sys.exit()

    if args.mcType.lower() != 'all' and args.mcType not in mc_campaigns() :
        print('Provided mcType is not in allowed list: {}'.format(mc_campaigns()))
        sys.exit()

    main(args)
